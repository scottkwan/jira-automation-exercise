class IssueFunction

 LOGIN_BUTTON  		= {link: 'Log In'}
 USER_NAME     		= {id: 'username'}
 PASSWORD		= {id: 'password'}
 LOGIN_SUBMIT		= {id: 'login-submit'}
 ISSUE_CREATE		= {id: 'create_link'}
 ISSUE_SUMMARY		= {id: 'summary'}
 ISSUE_SUBMIT		= {id: 'create-issue-submit'}
 ISSUE_DROP_DOWN	= {id: 'find_link'}
 ISSUE_SEARCH		= {id: 'issues_new_search_link_lnk'}
 ISSUE_SEARCH_TXT	= {id: 'searcher-query'}
 ISSUE_RESULT		= {id: 'summary-val'}
 ISSUE_EDIT		= {css: 'span.icon.jira-icon-edit'}
 ISSUE_EDIT_SAVE	= {id: 'edit-issue-submit'}
 USER_DETAIL		= {id: 'header-details-user-fullname'}
 LOGOUT			= {id: 'log_out'}
 BASE_URL 		= 'https://jira.atlassian.com/browse/TST'
 USER			= 'scottkwan@gmail.com'
 PASS			=  'Qatest2014'

 attr_reader :browser

 def initialize(browser)
  @browser=browser
 end

 def wait_for(seconds=10)
  Selenium::WebDriver::Wait.new(:timeout => seconds).until { yield }
 end

 def displayed?(web_element)
  browser.find_element(web_element).displayed?
    true
    rescue Selenium::WebDriver::Error::NoSuchElementError
    false
 end

 def login
  browser.get(BASE_URL)
  browser.find_element(LOGIN_BUTTON).click
  browser.find_element(USER_NAME).send_keys(USER)
  browser.find_element(PASSWORD).send_keys(PASS)
  browser.find_element(LOGIN_SUBMIT).click
 end

 def create_issue(create_name)
  wait_for {displayed?(ISSUE_CREATE)}
  browser.find_element(ISSUE_CREATE).click
  wait_for {displayed?(ISSUE_SUMMARY)}
  browser.find_element(ISSUE_SUMMARY).send_keys(create_name)
  browser.find_element(ISSUE_SUBMIT).click
  sleep 5
 end

 def search_issue(search_name)
  wait_for {displayed?(ISSUE_DROP_DOWN)}
  browser.find_element(ISSUE_DROP_DOWN).click
  wait_for {displayed?(ISSUE_SEARCH)}
  browser.find_element(ISSUE_SEARCH).click
  sleep 3
  wait_for {displayed?(ISSUE_SEARCH_TXT)}
  browser.find_element(ISSUE_SEARCH_TXT).send_keys(search_name)
  browser.find_element(ISSUE_SEARCH_TXT).send_keys :enter
  sleep 5
 end
 
 def edit_issue(edit_name)
  wait_for {displayed?(ISSUE_EDIT)}
  browser.find_element(ISSUE_EDIT).click
  wait_for {displayed?(ISSUE_SUMMARY)}
  browser.find_element(ISSUE_SUMMARY).clear
  browser.find_element(ISSUE_SUMMARY).send_keys(edit_name)
  browser.find_element(ISSUE_EDIT_SAVE).click
  sleep 5
 end

 def issue_result_found?(issue_name)
  wait_for {displayed?(ISSUE_RESULT)}
  browser.find_element(ISSUE_RESULT).text.include?(issue_name)
 end

 def logout
  browser.find_element(USER_DETAIL).click
  browser.find_element(LOGOUT).click
 end

end

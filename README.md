# README #

### What is this repository for? ###

This repository contains 2 ruby files. 
page_objects.rb - contains all the neccessary page objects and methods for the test to run
test.rb - contain the rspec test steps to run the required test scenarios

### How do I get set up? ###
Prerequisite:
1. gem install ruby
2. gem install rspec
3. gem install selenium-webdriver
4. firefox installed

How to run tests
1. clone repository
2. run "rspec test.rb"

### Notes from exercise ###
1.  I noticed the issue search is not too accurate.  If i search by issue id like TST-56852  it will not find it. Also if i search by summary name like "new edits" the first couple resutls will not be the issue that has the exact "new edits" summary name.  In my test I add double quotes to make sure it returns the exact issue name that i enter in the search.
require 'selenium-webdriver'
require 'rspec'
require_relative 'page_objects'

issue_to_create = 'Issue123'
issue_to_edit = 'Issue123'
issue_edit_to = 'edditted issue'
issue_to_search = 'new edits'

 def setup
  @browser = Selenium::WebDriver.for :firefox
 end

 def terminate
  @browser.quit
 end

 describe 'creating a new issue' do
   it 'will verify that a new issue was created' do
  	setup
	b = IssueFunction.new(@browser)
	b.login
	b.create_issue(issue_to_create)
	b.search_issue('"'+issue_to_create+'"')
	expect(b.issue_result_found?(issue_to_create)).to be true
	b.logout
 	terminate
   end
  end

  describe 'Editting an issue' do
    it 'will save the edits a user made' do
	setup
	b = IssueFunction.new(@browser)
	b.login
	b.search_issue('"'+issue_to_create+'"')
	b.edit_issue(issue_edit_to)
	b.search_issue('"'+issue_edit_to+'"')
	expect(b.issue_result_found?(issue_edit_to)).to be true
	b.logout
 	terminate
    end
  end

  describe 'Search for issue' do 
    it 'will return search results' do
	setup
	b = IssueFunction.new(@browser)
	b.login
	b.search_issue('"'+issue_to_search+'"')
	expect(b.issue_result_found?(issue_to_search)).to be true
	b.logout
 	terminate
    end
end

	



